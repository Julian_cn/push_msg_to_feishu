FROM    python:3.10.10-alpine

RUN     mkdir /app

COPY    ./requirements.txt /app

RUN     python3 -m pip install -i https://pypi.tuna.tsinghua.edu.cn/simple -r /app/requirements.txt

COPY    . /app

WORKDIR /app

CMD     ["python3", "/app/app.py"]