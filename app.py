#! python
# -*- coding: utf8 -*-


import os
import sys
import json
import yaml
import logging
from flask import Flask, request
from logging.handlers import RotatingFileHandler
# 飞书方法
from utils import feishu_card
from utils import feishu_robot

app = Flask(__name__)


load_config = open("config.yaml", mode="r", encoding="utf-8")
config = yaml.load(load_config, Loader=yaml.Loader)


if os.path.exists("logs"):
    pass
else:
    try:
        os.mkdir("logs")
    except Exception as e:
        print("Error: %s " % e)
        sys.exit(1)

# 创建日志记录器并设置级别
logger = logging.getLogger()
logger.setLevel(logging.INFO)

# 创建文件处理器并设置级别、文件大小和备份数量
file_handler = RotatingFileHandler('./logs/app.log', maxBytes=1024 * 1024, backupCount=10)
file_handler.setLevel(logging.INFO)

# 创建格式化器并添加到文件处理器
formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]')
file_handler.setFormatter(formatter)

# 添加文件处理器到日志记录器
logger.addHandler(file_handler)

# 添加终端处理器到日志记录器
stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.INFO)
logger.addHandler(stream_handler)


def get_request_body_data():
    byte_data = request.get_data()
    json_data = json.loads(byte_data.decode("utf-8").replace("'", '"'), strict=False)
    logger.info("请求 body : %s " % json_data)
    return json_data


def return_response(api_path):
    response = {
        "api": api_path,
        "code": 1,
        "msg": "Request mode uses POST"
    }
    return response


@app.route('/api/jenkins_build_info', methods=["POST", "GET"])
def jenkins_build_info():
    if request.method != "POST":
        return return_response(api_path="/api/jenkins_build_info")
    else:
        json_data = get_request_body_data()

        # 告警推送群组
        group = request.args.get("group")
        logger.info("请求 args : %s " % group)
        robot_url = feishu_robot.get_fei_shu_robot(push_platform_source="jenkins", group=group)
        logger.info("请求 URL : %s" % robot_url)
        project_name = json_data.get("project_name")
        fei_shu = feishu_card.CardMsg(project_name=project_name, url=robot_url)
        resp = fei_shu.send_msg_to_group(push_platform_source="jenkins", data=json_data)
        logger.info("推送 jenkins 构建消息 %s " % resp)
        response = {"code": 200, "api": "/api/jenkins_build_info", "msg": "Send fei,shu message success"}
        return response


@app.route('/api/alert_manager_msg', methods=["POST", "GET"])
def alert_manager_msg():
    if request.method != "POST":
        return return_response(api_path="/api/alert_manager_msg")
    else:
        json_data = get_request_body_data()
        # 告警群组
        alert_group = json_data.get("commonLabels").get("alert_group")
        robot_url = feishu_robot.get_fei_shu_robot(push_platform_source="prometheus", group=alert_group)
        fei_shu = feishu_card.CardMsg(url=robot_url)
        resp = fei_shu.send_msg_to_group(push_platform_source="prometheus", data=json_data)
        logger.info("推送 prometheus 告警消息 %s " % resp)
        response = {"code": 200, "api": "/api/alert_manager_msg", "msg": "Send fei,shu message success"}
        return response


@app.route('/', methods=["GET", "POST"])
def root():
    response = {"code": 200, "msg": "hello world"}
    return response


def main():
    # 日志记录
    app.debug = True
    if os.getenv("PORT"):
        app.run(host="0.0.0.0", port=int(os.getenv("PORT")))
    else:
        app.run(host="0.0.0.0", port=config.get("port"))


if __name__ == '__main__':
    main()
