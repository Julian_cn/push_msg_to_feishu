#! python
# -*- coding: utf8 -*-


import os
import yaml


load_config = open("config.yaml", mode="r", encoding="utf-8")
config = yaml.load(load_config, Loader=yaml.Loader)


def get_fei_shu_robot(push_platform_source, group):
    """
    获取 jenkins 构建推送消息地址
        1. 优先使用环境变量绝对匹配
        2. 其次使用配置文件绝对匹配
        3. 优先使用环境变量默认
        4. 其次使用配置文件默认
    :param push_platform_source: 推送消息平台来源
    :param group: 请求接口传递参数
    :return: robot_url
    """
    # URL 传递带有参数
    if group:
        env_robot_url = os.getenv("fei_shu." + push_platform_source + "." + group)
        env_default_url = os.getenv("fei_shu." + push_platform_source + ".url")
        config_robot_url = config.get("fei_shu").get(push_platform_source).get(group)
        config_default_robot_url = config.get("fei_shu").get(push_platform_source).get("url")
        # 优先获取环境变量
        if env_robot_url:
            return env_robot_url
        # 其次获取配置文件
        elif config_robot_url:
            return config_robot_url
        # 然后使用环境变量默认
        elif env_default_url:
            return env_default_url
        # 最后使用配置文件默认
        else:
            return config_default_robot_url
    # URL 没有参数传递
    else:
        env_robot_url = os.getenv("fei_shu." + push_platform_source + ".url")
        config_robot_url = config.get("fei_shu").get(push_platform_source).get("url")
        if env_robot_url:
            return env_robot_url
        else:
            return config_robot_url
