#! python
# -*- coding: utf8 -*-


import time
import json
import requests


class FormatData:
    def __init__(self, dict_data=None):
        self.dict_data = dict_data

    def format_label_msg(self, title):
        """
        格式化 dict 字段信息，添加 标题 信息
        :param title: 标题信息
        :return: 满足 feishu 机器人格式化后数据
        """
        result = "**{title}**\n".format(title=title)
        fields_str = ""
        for k, v in self.dict_data.items():
            if k and v:
                fields_str += "• {k}: {v} \n".format(k=k, v=v)
            else:
                result += fields_str
                return result
        result += fields_str
        return result


class CardTemplate:
    @staticmethod
    def _markdown_content(content: str, text_align=None):
        if text_align:
            md_content = {
                "tag": "markdown",
                "content": content,
                "text_align": text_align
            }
        else:
            md_content = {
                "tag": "markdown",
                "content": content
            }
        return md_content

    @staticmethod
    def _double_column_content(content_01: str, content_02: str):
        """双列文字"""
        fields_content = {
            "tag": "div",
            "fields": [
                {
                    "is_short": True,
                    "text": {
                        "tag": "lark_md",
                        "content": content_01
                    }
                },
                {
                    "is_short": True,
                    "text": {
                        "tag": "lark_md",
                        "content": content_02
                    }
                }
            ]
        }
        return fields_content

    @staticmethod
    def _content_button(content: str, button: dict):
        """内容+按钮"""
        text_button = {
            "tag": "div",
            "text": {
                "tag": "lark_md",
                "content": content
            },
            "extra": {
                "tag": "button",
                "text": {
                    "tag": "lark_md",
                    "content": button.get("content")
                },
                "type": "primary",
                "multi_url": {
                    "url": button.get("url"),
                }
            }
        }
        return text_button

    def jenkins_card_temp(self, data: dict):
        elements = list()
        elements.append({"tag": "hr"})
        now_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        elements.append(self._markdown_content("**⏳ 发布时间:** {build_time}".format(
          build_time=now_time)))
        elements.append(self._double_column_content(
          content_01="**👨 提交作者: **\n {commit_author_name}".format(
              commit_author_name=data.get("commit_author_name")),
          content_02="**📌 提交 ID: **\n {commit_id}".format(commit_id=data.get("commit_id"))
        ))
        elements.append(self._double_column_content(
          content_01="**🎯 构建版本: **\n [{build_number}]({jenkins_url})".format(
            build_number=data.get("build_number"),
            jenkins_url=data.get("jenkins_url")
          ),
          content_02="**⏱ 提交时间: **\n {commit_date}".format(commit_date=data.get("commit_date"))
        ))
        elements.append(self._content_button(
          content="**📝 更新信息: **\n {msg}".format(msg=data.get("msg")),
          button={
            "content": "查看详情",
            "url": data.get("commit_url")
          }
        ))
        return elements

    def prometheus_card_temp(self, alert_data):
        """
        卡片模版；告警平台，时间，状态，项目，数值
        :return: 发送数据模板
        """
        elements = list()
        now_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        elements.append(self._double_column_content(
            content_01="**🕐 时间：**\n %s " % now_time,
            content_02="**🔢 状态：**\n %s " % alert_data.get("status")
        ))
        format_data = FormatData(dict_data=alert_data.get("alerts")[0].get("labels"))
        # 标签: label
        elements.append(self._markdown_content("{title}".format(
            title=format_data.format_label_msg(title="⏳ 服务标签:"))))
        # 详情: annotations
        elements.append(self._markdown_content("**💬 详细信息:** \n {description} \n {summary}".format(
            description="description: " + alert_data.get("alerts")[0].get("annotations").get("description"),
            summary="summary: " + alert_data.get("alerts")[0].get("annotations").get("summary"),
        )))

        return elements


class CardMsg(CardTemplate):
    def __init__(self, url, project_name=None):
        super().__init__()
        # 请求头参数
        self.headers = {
            "content_type": "application/json; charset=utf-8"
        }
        # 请求 url
        self.url = url
        self.project_name = project_name

    @staticmethod
    def _header(title: str, template="indigo"):
        header = {
            "template": template,
            "title": {
                "content": title,
                "tag": "plain_text"
            }
        }
        return header

    def jenkins_build_info_card(self, build_data: dict):
        """构建历史记录卡片"""
        content = {
            "config": {
                "wide_screen_mode": True,
                "update_multi": True
            },
            "elements": CardTemplate().jenkins_card_temp(data=build_data),
            "header": self._header("%s 构建信息" % self.project_name)
        }

        data = {
            "msg_type": "interactive",
            "card": str(json.dumps(content))
        }
        print(json.dumps(content))
        return data

    def prometheus_alert_info_card(self, alert_data):

        alert_status = alert_data.get("status")
        if alert_status == "firing":
            template = "red"
        elif alert_status == "resolved":
            template = "green"
        else:
            template = "indigo"

        header = self._header(title="Prometheus 告警信息", template=template)
        content = {
            "config": {
                "wide_screen_mode": True
            },
            "elements": CardTemplate().prometheus_card_temp(alert_data=alert_data),
            "header": header
        }

        data = {
            "msg_type": "interactive",
            "card": str(json.dumps(content))
        }
        print(json.dumps(content))
        return data

    def send_msg_to_group(self, push_platform_source: str, data: dict):
        """
        发送消息飞书群
        :param push_platform_source: 消息来源
        :param data: 发送飞书数据
        """
        if push_platform_source == "jenkins":
            send_data = self.jenkins_build_info_card(build_data=data)
        elif push_platform_source == "prometheus":
            send_data = self.prometheus_alert_info_card(alert_data=data)
        else:
            send_data = ""
        response = requests.post(
            url=self.url,
            headers=self.headers,
            data=send_data)
        resp = response.content.decode("utf-8")
        return json.loads(resp)
