# 项目名称

推送 Jenkins 构建信息至飞书群组

## 项目介绍

该项目是一个推送消息的程序，可以将 Jenkins 构建的信息推送至飞书群组。使用该程序可以方便地将 Jenkins 构建的结果及时通知到相关人员，提高团队协作效率。

## 开源许可证

该项目采用 MIT 许可证开源，任何人都可以自由地使用、修改和分发该项目的代码。

## 使用方法
### 源码运行
1. 下载代码

   ```bash
   git clone https://gitee.com/Julian_cn/push_msg_to_feishu.git
   ```

2. 安装依赖

   ```bash
   pip install -r requirements.txt
   ```

3. 配置飞书群组

   在飞书中创建一个机器人，并获取机器人的 Webhook 地址。

4. 配置程序

   在 `config.py` 文件中配置飞书的机器人地址参数。

5. 运行程序

   ```
   python app.py
   ```

### 容器启动

#### docker
1. 启动目录

   进入 `deployment/docker-compose` 目录
   
2. 修改环境变量 `docker-compose.yaml`

   ```bash
   fei_shu.jenkins.url: "修改为你要推送的地址"
   ```

3. 启动项目
   ```bash
   docker-compose up -d
   ```

#### k8s
1. 启动目录

   进入 `deployment/k8s/push-msg-to-feishu` 目录
   
2. 修改环境变量 `values.yaml`
   - 环境变量

      ```bash
      fei_shu.jenkins.url: "修改为你要推送的地址"
      ```

   - ingress 配置
      ```yaml
      ingress:
        # 开启 ingress 配置
        enabled: true
        className: ""
        annotations: {}
          # kubernetes.io/ingress.class: nginx
          # kubernetes.io/tls-acme: "true"
        hosts:
          # 将域名改为你想指定的域名
          - host: push-msg-to-feishu.liyan-zhu.cn
            paths:
              - path: /
                pathType: ImplementationSpecific
        # 证书配置，可选
        tls: [] 
      ```

3. 启动项目
   ```bash
   cd deployment/k8s/push-msg-to-feishu
   
   helm upgrade -i push-msg-to-feishu
   ```

## 代码结构

```
.
├── app.py
├── config.yaml
├── deployment
│   ├── docker-compose
│   │   └── docker-compose.yaml
│   └── k8s
│       └── push-msg-to-feishu
│           ├── Chart.yaml
│           ├── templates
│           │   ├── deployment.yaml
│           │   ├── _helpers.tpl
│           │   ├── ingress.yaml
│           │   ├── NOTES.txt
│           │   ├── serviceaccount.yaml
│           │   └── service.yaml
│           └── values.yaml
├── Dockerfile
├── Jenkinsfile
├── LICENSE
├── README.md
├── requirements.txt
└── utils
    ├── feishu_card.py
    ├── feishu_robot.py
    └── utils.py
```

- `app.py`：程序的入口文件，定义了接口路由和消息推送的逻辑。
- `config.py`：配置文件，包含了飞书的相关参数。
- `deployment`：部署文件，Docker 和 K8s 部署方式。
- `utils`：功能模块，飞书卡片生成功能等。
- `requirements.txt`：程序依赖的 Python 包列表。


## 贡献者

- @Julian_cn

## 版本历史

- v0.0.1：实现了将 Jenkins 构建信息推送至飞书群组的功能。
- v0.0.2：实现了将 Prometheus 告警信息推送至飞书群组的功能。

## 参考文献

- [飞书机器人开发文档](https://open.feishu.cn/document/ukTMukTMukTM/ucTM5YjL3ETO24yNxkjN)